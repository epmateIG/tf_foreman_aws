variable "ami" {
  default = null
}

variable "ami_default_user" {
  default = "centos"
}

variable "global_root_disk_size" {
  description = "Global setting for root disk size for all nodes"
  default     = 50
}

variable "global_root_disk_type" {
  default = "standard"
}

variable "encrypted_root" {
  default = true
}

variable "vpc_cidr" {
  description = "CIDR block to assign to customer VPC"
  default     = "198.18.0.0/16"
}

variable "core_instance_type" {
  description = "The AMI to use for the core nodes"
  default     = "c5.2xlarge"
}

variable "core_instance_startip" {
  description = "First IP for core nodes"
  default     = 0
}

variable "core_root_disk_size" {
  description = "Root disk size for core nodes"
  default     = null
}

variable "core_root_disk_type" {
  description = "Root disk type for core nodes"
  default     = null
}

variable "mongo_instance_type" {
  description = "The AMI to use for the MongoDB nodes"
  default     = "c5.4xlarge"
}

variable "mongo_instance_startip" {
  description = "First IP for mongo nodes"
  default     = 0
}

variable "mongo_root_disk_size" {
  description = "Root disk size for mongo nodes"
  default     = null
}

variable "mongo_root_disk_type" {
  description = "Root disk type for mongo nodes"
  default     = null
}

variable "kubemaster_instance_type" {
  description = "The AMI to use for the kubernetes master nodes"
  default     = "c5.xlarge"
}

variable "kubemaster_instance_startip" {
  description = "First IP for kubemaster nodes"
  default     = 0
}

variable "kubemaster_root_disk_size" {
  description = "Root disk size for kubemaster nodes"
  default     = null
}

variable "kubemaster_root_disk_type" {
  description = "Root disk type for kubemaster nodes"
  default     = null
}

variable "kubeworker_instance_type" {
  description = "The AMI to use for the kubernetes worker nodes"
  default     = "c5.4xlarge"
}

variable "iam_kubeworker_role" {
  description = "The IAM role to apply on the kubernetes worker nodes"
  default     = null
}

variable "kubeworker_instance_startip" {
  description = "First IP for kubeworker nodes"
  default     = 0
}

variable "kubeworker_root_disk_size" {
  description = "Root disk size for kubeworker nodes"
  default     = null
}

variable "kubeworker_root_disk_type" {
  description = "Root disk type for kubeworker nodes"
  default     = null
}

variable "loadbalancer_instance_type" {
  description = "The AMI to use for the loadbalancer nodes"
  default     = "m5.large"
}

variable "loadbalancer_instance_startip" {
  description = "First IP for loadbalancer nodes"
  default     = 0
}

variable "lb_root_disk_size" {
  description = "Root disk size for loadbalancer nodes"
  default     = null
}

variable "lb_root_disk_type" {
  description = "Root disk type for loadbalancer nodes"
  default     = null
}

variable "agent_pub_instance_type" {
  description = "The AMI to use for the public agent nodes"
  default     = "m5.large"
}

variable "agent_pub_instance_startip" {
  description = "First IP for public agent nodes"
  default     = 0
}

variable "agent_pub_root_disk_size" {
  description = "Root disk size for public agent nodes"
  default     = null
}

variable "agent_pub_root_disk_type" {
  description = "Root disk type for public agent nodes"
  default     = null
}

variable "agent_private_instance_type" {
  description = "The AMI to use for the private agent nodes"
  default     = "m5.large"
}

variable "agent_private_instance_startip" {
  description = "First IP for private agent nodes"
  default     = 0
}

variable "agent_private_root_disk_size" {
  description = "Root disk size for private agent nodes"
  default     = null
}

variable "agent_private_root_disk_type" {
  description = "Root disk type for public agent nodes"
  default     = null
}

variable "elastic_lb_count" {
  description = "The elastic loadbalancer count for the whole platform. The value must be either 0 or 1"
  default     = 1
}

variable "mongo_nodes_count" {
  description = "The nodes count for the MongoDB. The value must be odd, like 1, 3 or 5"
  default     = 3
}

variable "core_nodes_count" {
  description = "The nodes count for the core software (Karaf). Can be odd or even."
  default     = 2
}

variable "loadbalancer_nodes_count" {
  description = "The nodes count for the loadbalancers. Can be odd or even."
  default     = 2
}

variable "kubemaster_nodes_count" {
  description = "The nodes count for the kubernetes cluster masters. The value must be odd."
  default     = 3
}

variable "kubeworker_nodes_count" {
  description = "The nodes count for the kubernetes workers. Can be odd or even."
  default     = 2
}

variable "agent_pub_nodes_count" {
  description = "The nodes count for the public agents. Can be odd or even."
  default     = 0
}

variable "agent_private_nodes_count" {
  description = "The nodes count for the private agents. Can be odd or even."
  default     = 0
}

variable "availability_zones" {
  description = "The number of availability zones to use"
  default     = 3
}

variable "az_names" {
  description = "The availability zone names to use (default auto)"
  default     = null
}

variable "associate_public_ip" {
  default = false
}

variable "keypair_algorithm" {
  default = "RSA"
}

variable "vpc_id" {
  description = "ID of VPC to use"
  default     = null
}

variable "mgmt_vpc_id" {
  description = "ID of management VPC to peer"
  default     = null
}

variable "mgmt_vpc_region" {
  description = "region where management VPC to peer is located"
  default     = null
}

variable "mgmt_vpc_cidr" {
  description = "CIDR of management VPC to peer"
  default     = null
}

variable "mgmt_vpc_rtables" {
  description = "Route tables of management VPC to peer"
  default     = []
}

variable "route_table_igw" {
  description = "Routing table using internet gateway (public IP must be assigned on hosts)"
  default     = null
}

variable "route_table_nat" {
  description = "Routing table using nat gateway"
  default     = null
}

variable "global_subnet_start_iprange" {
  description = "Global CIDR notation of a starting IP range"
  default     = null
}

variable "data_subnet_start_iprange" {
  description = "Data CIDR notation of a starting IP range"
  default     = null
}

variable "compute_subnet_start_iprange" {
  description = "Compute CIDR notation of a starting IP range"
  default     = null
}

variable "lb_subnet_start_iprange" {
  description = "Loadbalancer CIDR notation of a starting IP range"
  default     = null
}

variable "mgmt_subnet_length" {
  default = 28
}

variable "subnet_length" {
  default = 26
}

variable "keypair" {
  default     = null
  description = "A keypair name or ID to use"
}

variable "loadbalancer_ports" {
  description = "A list of maps for ingress port + protocol matches"

  default = [
    {
      proto       = -1
      port        = 0
      cidr_blocks = "0.0.0.0/0"
      direction   = "egress"
    },
    {
      proto       = "tcp"
      port        = 80
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 443
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 1883
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 8111
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 8883
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 1884
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
  ]
}

variable "datahub_lb_ports" {
  description = "A list of maps for ingress port + protocol matches"

  default = [
    {
      proto       = "tcp"
      port        = 9047
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 9447
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
    {
      proto       = "tcp"
      port        = 31010
      cidr_blocks = "0.0.0.0/0"
      direction   = "ingress"
    },
  ]
}

variable "core_ports" {
  description = "A list of maps for ingress port + protocol matches"
  default     = null
}

variable "mongo_ports" {
  description = "A list of maps for ingress port + protocol matches"
  default     = null
}

variable "kubemaster_ports" {
  description = "A list of maps for ingress port + protocol matches"
  default     = null
}

variable "kubeworker_ports" {
  description = "A list of maps for ingress port + protocol matches"
  default     = null
}

variable "agent_pub_ports" {
  description = "A list of maps for ingress port + protocol matches"
  default     = null
}

variable "agent_private_ports" {
  description = "A list of maps for ingress port + protocol matches"
  default     = null
}

variable "internal_communication_ports" {
  description = "Global rules applied to the hosts (except loadbalancer) if no more specific rules were defined"
  default     = null
}

variable "ssh_port" {
  default = 22
}

variable "core_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "mongo_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sde"
    },
    {
      name       = "MONGO"
      mountpoint = "/opt/mongodb"
      size       = 64
      type       = "gp2"
      filesystem = "xfs"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "kubemaster_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "kubeworker_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "loadbalancer_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "agent_pub_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "agent_private_aux_vols" {
  default = [
    {
      name       = "VAR"
      mountpoint = "/var"
      size       = 32
      type       = "gp2"
      filesystem = "ext4"
      encrypt    = true
      device     = "/dev/sdf"
    }
  ]
}

variable "ssh_allowed_cidr_blocks" {
  default = null
}

variable "user_supplied_security_groups" {
  description = "This list will be additionaly concatenated with terraform-provided security groups"
  default     = []
}

variable "customer_name" {
  default     = null
  description = "Customer name that will be used in a 'Customer' tag"
}

variable "kms_key_id" {
  default     = null
  description = "ARN-formatted KMS key ID to use for encryption. Do not specify for not encrypted volumes."
}

variable "elb_idle_timeout" {
  default = 4000
}

variable "elb_drain_timeout" {
  default = 300
}

variable "datahub" {
  default = false
}

variable "cumulocity_elb_listeners" {
  default = [
    {
      instance_port     = 80
      instance_protocol = "tcp"
      lb_port           = 80
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 443
      instance_protocol = "tcp"
      lb_port           = 443
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 1883
      instance_protocol = "tcp"
      lb_port           = 1883
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 8111
      instance_protocol = "tcp"
      lb_port           = 8111
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 8883
      instance_protocol = "tcp"
      lb_port           = 8883
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 1884
      instance_protocol = "tcp"
      lb_port           = 1884
      lb_protocol       = "tcp"
    }
  ]
}

variable "datahub_elb_listeners" {
  default = [
    {
      instance_port     = 9047
      instance_protocol = "tcp"
      lb_port           = 9047
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 9447
      instance_protocol = "tcp"
      lb_port           = 9447
      lb_protocol       = "tcp"
    },
    {
      instance_port     = 31010
      instance_protocol = "tcp"
      lb_port           = 31010
      lb_protocol       = "tcp"
    }
  ]
}

variable "user_supplied_tags" {
  default = {}
}
