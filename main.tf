provider "aws" {
  alias  = "peer"
  region = local.mgmt_vpc_region
}

data "aws_ami" "centos_latest" {
  owners      = ["679593333241"]
  most_recent = true

  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "product-code"
    values = ["aw0evgkw8e5c1q413zgy5pjce"]
  }
}

data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_kms_key" "enc_key" {
  count  = local.kms_key_id_is_alias ? 1 : 0
  key_id = format("alias/%s", var.kms_key_id)
}

data "aws_route_table" "igw" {
  count  = var.route_table_igw != null || var.vpc_id == null ? 0 : 1
  vpc_id = local.vpc_id
  tags   = { tf_select_igw = true }
}

data "aws_route_table" "nat" {
  count  = var.route_table_nat != null || var.vpc_id == null ? 0 : 1
  vpc_id = local.vpc_id
  tags   = { tf_select_nat = true }
}

data "aws_caller_identity" "current" {}

data "aws_vpc" "mgmt_vpc_auto" {
  provider = aws.peer
  count    = var.mgmt_vpc_id == "auto" ? 1 : 0
  tags     = { tf_select_mgmt_vpc = true }
}

data "aws_vpc" "mgmt_vpc" {
  provider = aws.peer
  count    = local.mgmt_vpc_id != null ? 1 : 0
  id       = local.mgmt_vpc_id
}

resource "aws_key_pair" "kp" {
  count      = var.keypair != null ? 0 : 1
  key_name   = format("%s-kp", terraform.workspace)
  public_key = tls_private_key.pk[0].public_key_openssh
}

resource "tls_private_key" "pk" {
  count     = var.keypair != null ? 0 : 1
  algorithm = var.keypair_algorithm
}

resource "aws_vpc" "vpc" {
  count      = var.vpc_id != null ? 0 : 1
  cidr_block = var.vpc_cidr
  tags = merge({
    Name     = format("%s-vpc", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_internet_gateway" "internet_gw" {
  count  = var.vpc_id != null ? 0 : 1
  vpc_id = aws_vpc.vpc[0].id
  tags = merge({
    Name     = format("%s-igw", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_route" "igw_route" {
  count                  = var.vpc_id != null ? 0 : 1
  route_table_id         = aws_vpc.vpc[0].default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gw[0].id
}

resource "aws_eip" "natgw_elastic_ip" {
  count = var.vpc_id != null ? 0 : 1
  vpc   = true
  tags = merge({
    Name     = format("%s-nat-eip", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_subnet" "management" {
  count  = var.vpc_id != null ? 0 : 1
  vpc_id = aws_vpc.vpc[0].id
  cidr_block = cidrsubnet(
    var.global_subnet_start_iprange,
    var.mgmt_subnet_length - split("/", var.global_subnet_start_iprange)[1],
    pow(2, var.mgmt_subnet_length - split("/", var.global_subnet_start_iprange)[1]) - 1
  )
  availability_zone       = element(local.az_names, count.index)
  map_public_ip_on_launch = var.associate_public_ip

  tags = merge({
    Name     = format("%s-management-subnet", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_nat_gateway" "vpc_nat_gw" {
  count         = var.vpc_id != null ? 0 : 1
  allocation_id = aws_eip.natgw_elastic_ip[0].id
  subnet_id     = aws_subnet.management[0].id

  tags = merge({
    Name     = format("%s-nat-gw", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_route_table" "natgw_rtb" {
  count  = var.vpc_id != null ? 0 : 1
  vpc_id = local.vpc_id

  tags = merge({
    Name     = format("%s-nat-rtb", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_route" "natgw_route" {
  count                  = var.vpc_id != null ? 0 : 1
  route_table_id         = aws_route_table.natgw_rtb[0].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.vpc_nat_gw[0].id
}

resource "aws_elb" "elb" {
  count   = var.loadbalancer_nodes_count > 0 ? var.elastic_lb_count : 0
  name    = format("%s-elb", var.customer_name != null ? var.customer_name : terraform.workspace)
  subnets = aws_subnet.lb.*.id

  dynamic "listener" {
    for_each = local.listener_list
    content {
      instance_port     = listener.value["instance_port"]
      instance_protocol = listener.value["instance_protocol"]
      lb_port           = listener.value["lb_port"]
      lb_protocol       = listener.value["lb_protocol"]
    }
  }

  instances                   = aws_instance.loadbalancer.*.id
  cross_zone_load_balancing   = true
  idle_timeout                = var.elb_idle_timeout
  connection_draining         = true
  connection_draining_timeout = var.elb_drain_timeout

  security_groups = [aws_security_group.loadbalancer[0].id]

  tags = merge({
    Name     = "tf-elb"
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_eip" "agent_pub" {
  count    = var.agent_pub_nodes_count
  instance = aws_instance.agent_pub.*.id[count.index]
  vpc      = true
}

resource "aws_subnet" "data" {
  count  = var.availability_zones
  vpc_id = local.vpc_id
  cidr_block = cidrsubnet(
    local.data_subnet_start_iprange,
    local.data_subnet_newbits,
    var.data_subnet_start_iprange != null ? count.index : count.index + var.availability_zones * 2,
  )
  availability_zone       = element(local.az_names, count.index)
  map_public_ip_on_launch = var.associate_public_ip

  tags = merge({
    Name = format(
      "%s-data-subnet-%s",
      terraform.workspace,
      substr(element(local.az_names, count.index), -1, 1),
    )
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_subnet" "compute" {
  count  = var.availability_zones
  vpc_id = local.vpc_id
  cidr_block = cidrsubnet(
    local.compute_subnet_start_iprange,
    local.compute_subnet_newbits,
    var.compute_subnet_start_iprange != null ? count.index : count.index + var.availability_zones,
  )
  availability_zone       = element(local.az_names, count.index)
  map_public_ip_on_launch = var.associate_public_ip

  tags = merge({
    Name = format(
      "%s-compute-subnet-%s",
      terraform.workspace,
      substr(element(local.az_names, count.index), -1, 1),
    )
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_subnet" "lb" {
  count  = var.availability_zones
  vpc_id = local.vpc_id
  cidr_block = cidrsubnet(
    local.lb_subnet_start_iprange,
    local.lb_subnet_newbits,
    count.index,
  )
  availability_zone       = element(local.az_names, count.index)
  map_public_ip_on_launch = true

  tags = merge({
    Name = format(
      "%s-lb-subnet-%s",
      terraform.workspace,
      substr(element(local.az_names, count.index), -1, 1),
    )
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_route_table_association" "data_rt" {
  count          = var.availability_zones
  subnet_id      = element(aws_subnet.data.*.id, count.index)
  route_table_id = var.associate_public_ip == true ? local.route_table_igw[0] : local.route_table_nat[0]
}

resource "aws_route_table_association" "compute_rt" {
  count          = var.availability_zones
  subnet_id      = element(aws_subnet.compute.*.id, count.index)
  route_table_id = var.associate_public_ip == true ? local.route_table_igw[0] : local.route_table_nat[0]
}

resource "aws_route_table_association" "lb_rt" {
  count          = var.availability_zones
  subnet_id      = element(aws_subnet.lb.*.id, count.index)
  route_table_id = local.route_table_igw[0]
}

resource "aws_iam_role" "iam_kubeworker_role" {
  count              = local.apply_roles && var.iam_kubeworker_role == null ? 1 : 0
  name               = format("%s-Kubernetes-IAM-Role", terraform.workspace)
  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  EOF

  tags = merge({
    Name     = format("%s-kubeworker-%03d", terraform.workspace, count.index + 1)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_iam_instance_profile" "iam_kubeworker_profile" {
  count = local.apply_roles && var.iam_kubeworker_role == null ? 1 : 0
  name  = format("%s-Kubernetes-IAM-Profile", terraform.workspace)
  role  = aws_iam_role.iam_kubeworker_role[0].name
}

resource "aws_instance" "core" {
  count             = var.core_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.core_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.core[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = var.associate_public_ip
  subnet_id                   = element(aws_subnet.compute.*.id, count.index)

  root_block_device {
    volume_type = local.core_root_disk_type
    volume_size = local.core_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.core_instance_startip != 0 ? cidrhost(
    element(aws_subnet.compute.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.core_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.core_aux_vols })

  tags = merge({
    Name              = format("%s-core-%03d", terraform.workspace, count.index + 1)
    Customer          = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_cores     = count.index + 1
    c8yType_kube-node = count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_instance" "loadbalancer" {
  count             = var.loadbalancer_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.loadbalancer_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.loadbalancer[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = true
  subnet_id                   = element(aws_subnet.lb.*.id, count.index)

  root_block_device {
    volume_type = local.lb_root_disk_type
    volume_size = local.lb_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.loadbalancer_instance_startip != 0 ? cidrhost(
    element(aws_subnet.lb.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.loadbalancer_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.loadbalancer_aux_vols })

  tags = merge({
    Name                  = format("%s-loadbalancer-%03d", terraform.workspace, count.index + 1)
    Customer              = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_loadbalancers = count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, associate_public_ip_address, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_instance" "mongo" {
  count             = var.mongo_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.mongo_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.mongo[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = var.associate_public_ip
  subnet_id                   = element(aws_subnet.data.*.id, count.index)

  root_block_device {
    volume_type = local.mongo_root_disk_type
    volume_size = local.mongo_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.mongo_instance_startip != 0 ? cidrhost(
    element(aws_subnet.data.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.mongo_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.mongo_aux_vols })

  tags = merge({
    Name                  = format("%s-mongo-%03d", terraform.workspace, count.index + 1)
    Customer              = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_mongodb_nodes = count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_instance" "kubemaster" {
  count             = var.kubemaster_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.kubemaster_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.kubemaster[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = var.associate_public_ip
  subnet_id                   = element(aws_subnet.compute.*.id, count.index)

  root_block_device {
    volume_type = local.kubemaster_root_disk_type
    volume_size = local.kubemaster_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.kubemaster_instance_startip != 0 ? cidrhost(
    element(aws_subnet.compute.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.kubemaster_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.kubemaster_aux_vols })

  tags = merge({
    Name                = format("%s-kubemaster-%03d", terraform.workspace, count.index + 1)
    Customer            = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_kube-master = count.index + 1
    c8yType_k8s-cluster = count.index + 1
    c8yType_etcd        = count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_instance" "kubeworker" {
  count             = var.kubeworker_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.kubeworker_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.kubeworker[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = var.associate_public_ip
  subnet_id                   = element(aws_subnet.compute.*.id, count.index)

  root_block_device {
    volume_type = local.kubeworker_root_disk_type
    volume_size = local.kubeworker_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.kubeworker_instance_startip != 0 ? cidrhost(
    element(aws_subnet.compute.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.kubeworker_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.kubeworker_aux_vols })

  iam_instance_profile = local.iam_kubeworker_profile

  tags = merge({
    Name                = format("%s-kubeworker-%03d", terraform.workspace, count.index + 1)
    Customer            = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_kube-node   = var.core_nodes_count + count.index + 1
    c8yType_k8s-cluster = var.kubemaster_nodes_count + count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_instance" "agent_pub" {
  count             = var.agent_pub_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.agent_pub_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.agent_pub[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = true
  subnet_id                   = element(aws_subnet.lb.*.id, count.index)

  root_block_device {
    volume_type = local.agent_pub_root_disk_type
    volume_size = local.agent_pub_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.agent_pub_instance_startip != 0 ? cidrhost(
    element(aws_subnet.lb.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.agent_pub_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.agent_pub_aux_vols })

  tags = merge({
    Name              = format("%s-agent_pub-%03d", terraform.workspace, count.index + 1)
    Customer          = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_agent-pub = count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, associate_public_ip_address, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_instance" "agent_private" {
  count             = var.agent_private_nodes_count
  ami               = var.ami != null ? var.ami : data.aws_ami.centos_latest.id
  availability_zone = element(local.az_names, count.index)
  instance_type     = var.agent_private_instance_type
  key_name          = local.keypair
  vpc_security_group_ids = concat(
    [aws_security_group.agent_private[0].id],
    [aws_security_group.ssh.id],
    var.user_supplied_security_groups,
  )
  associate_public_ip_address = var.associate_public_ip
  subnet_id                   = element(aws_subnet.compute.*.id, count.index)

  root_block_device {
    volume_type = local.agent_private_root_disk_type
    volume_size = local.agent_private_root_disk_size
    encrypted   = var.encrypted_root
    kms_key_id  = local.kms_key_id
  }

  private_ip = var.agent_private_instance_startip != 0 ? cidrhost(
    element(aws_subnet.compute.*.cidr_block, count.index),
    floor(count.index / var.availability_zones + var.agent_private_instance_startip),
  ) : ""

  user_data = templatefile("${path.module}/user_data.tmpl", { volumes = var.agent_private_aux_vols })

  tags = merge({
    Name                  = format("%s-agent_private-%03d", terraform.workspace, count.index + 1)
    Customer              = var.customer_name != null ? var.customer_name : terraform.workspace
    c8yType_agent-private = count.index + 1
  }, var.user_supplied_tags)

  lifecycle {
    ignore_changes        = [ami, ebs_optimized, user_data]
    create_before_destroy = true
  }
}

resource "aws_security_group" "loadbalancer" {
  count       = var.loadbalancer_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-loadbalancer-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "loadbalancer_rules" {
  count             = var.loadbalancer_nodes_count > 0 ? length(var.loadbalancer_ports) : 0
  type              = var.loadbalancer_ports[count.index]["direction"]
  from_port         = var.loadbalancer_ports[count.index]["port"]
  cidr_blocks       = split(",", var.loadbalancer_ports[count.index]["cidr_blocks"])
  to_port           = var.loadbalancer_ports[count.index]["port"]
  protocol          = var.loadbalancer_ports[count.index]["proto"]
  security_group_id = aws_security_group.loadbalancer[0].id
}

resource "aws_security_group_rule" "datahub_lb_rules" {
  count             = var.datahub && var.loadbalancer_nodes_count > 0 ? length(var.datahub_lb_ports) : 0
  type              = var.datahub_lb_ports[count.index]["direction"]
  from_port         = var.datahub_lb_ports[count.index]["port"]
  cidr_blocks       = split(",", var.datahub_lb_ports[count.index]["cidr_blocks"])
  to_port           = var.datahub_lb_ports[count.index]["port"]
  protocol          = var.datahub_lb_ports[count.index]["proto"]
  security_group_id = aws_security_group.loadbalancer[0].id
}

resource "aws_security_group" "core" {
  count       = var.core_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-core-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "core_rules" {
  count             = var.core_nodes_count > 0 ? length(local.core_ports) : 0
  type              = local.core_ports[count.index]["direction"]
  from_port         = local.core_ports[count.index]["port"]
  cidr_blocks       = split(",", local.core_ports[count.index]["cidr_blocks"])
  to_port           = local.core_ports[count.index]["port"]
  protocol          = local.core_ports[count.index]["proto"]
  security_group_id = aws_security_group.core[0].id
}

resource "aws_security_group" "mongo" {
  count       = var.mongo_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-mongo-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "mongo_rules" {
  count             = var.mongo_nodes_count > 0 ? length(local.mongo_ports) : 0
  type              = local.mongo_ports[count.index]["direction"]
  from_port         = local.mongo_ports[count.index]["port"]
  cidr_blocks       = split(",", local.mongo_ports[count.index]["cidr_blocks"])
  to_port           = local.mongo_ports[count.index]["port"]
  protocol          = local.mongo_ports[count.index]["proto"]
  security_group_id = aws_security_group.mongo[0].id
}

resource "aws_security_group" "kubemaster" {
  count       = var.kubemaster_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-kubemaster-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "kubemaster_rules" {
  count             = var.kubemaster_nodes_count > 0 ? length(local.kubemaster_ports) : 0
  type              = local.kubemaster_ports[count.index]["direction"]
  from_port         = local.kubemaster_ports[count.index]["port"]
  cidr_blocks       = split(",", local.kubemaster_ports[count.index]["cidr_blocks"])
  to_port           = local.kubemaster_ports[count.index]["port"]
  protocol          = local.kubemaster_ports[count.index]["proto"]
  security_group_id = aws_security_group.kubemaster[0].id
}

resource "aws_security_group" "kubeworker" {
  count       = var.kubeworker_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-kubeworker-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "kubeworker_rules" {
  count             = var.kubeworker_nodes_count > 0 ? length(local.kubeworker_ports) : 0
  type              = local.kubeworker_ports[count.index]["direction"]
  from_port         = local.kubeworker_ports[count.index]["port"]
  cidr_blocks       = split(",", local.kubeworker_ports[count.index]["cidr_blocks"])
  to_port           = local.kubeworker_ports[count.index]["port"]
  protocol          = local.kubeworker_ports[count.index]["proto"]
  security_group_id = aws_security_group.kubeworker[0].id
}

resource "aws_security_group" "agent_pub" {
  count       = var.agent_pub_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-agent_pub-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "agent_pub_rules" {
  count             = var.agent_pub_nodes_count > 0 ? length(local.agent_pub_ports) : 0
  type              = local.agent_pub_ports[count.index]["direction"]
  from_port         = local.agent_pub_ports[count.index]["port"]
  cidr_blocks       = split(",", local.agent_pub_ports[count.index]["cidr_blocks"])
  to_port           = local.agent_pub_ports[count.index]["port"]
  protocol          = local.agent_pub_ports[count.index]["proto"]
  security_group_id = aws_security_group.agent_pub[0].id
}

resource "aws_security_group" "agent_private" {
  count       = var.agent_private_nodes_count > 0 ? 1 : 0
  name_prefix = format("%s-agent_private-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "agent_private_rules" {
  count             = var.agent_private_nodes_count > 0 ? length(local.agent_private_ports) : 0
  type              = local.agent_private_ports[count.index]["direction"]
  from_port         = local.agent_private_ports[count.index]["port"]
  cidr_blocks       = split(",", local.agent_private_ports[count.index]["cidr_blocks"])
  to_port           = local.agent_private_ports[count.index]["port"]
  protocol          = local.agent_private_ports[count.index]["proto"]
  security_group_id = aws_security_group.agent_private[0].id
}

resource "aws_security_group" "ssh" {
  name_prefix = format("%s-ssh-", terraform.workspace)
  vpc_id      = local.vpc_id

  tags = merge({
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_security_group_rule" "ssh_rules" {
  count             = length(local.ssh_ports)
  type              = local.ssh_ports[count.index]["direction"]
  from_port         = local.ssh_ports[count.index]["port"]
  cidr_blocks       = split(",", local.ssh_ports[count.index]["cidr_blocks"])
  to_port           = local.ssh_ports[count.index]["port"]
  protocol          = local.ssh_ports[count.index]["proto"]
  security_group_id = aws_security_group.ssh.id
}

resource "random_shuffle" "az_names" {
  input        = data.aws_availability_zones.available.names
  result_count = var.availability_zones
}

resource "aws_vpc_peering_connection" "mgmt_vpc_peering" {
  count         = local.mgmt_vpc_id != null ? 1 : 0
  peer_owner_id = data.aws_caller_identity.current.account_id
  peer_vpc_id   = local.mgmt_vpc_id
  vpc_id        = local.vpc_id
  peer_region   = local.peer_in_same_region ? null : var.mgmt_vpc_region
  auto_accept   = local.peer_in_same_region

  tags = merge({
    Name     = format("%s-vpc", terraform.workspace)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
  }, var.user_supplied_tags)
}

resource "aws_vpc_peering_connection_accepter" "peer" {
  count                     = local.mgmt_vpc_id != null && ! local.peer_in_same_region ? 1 : 0
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.mgmt_vpc_peering[0].id
  auto_accept               = true
}

resource "aws_route" "natrt_mgmt_route" {
  count                     = local.mgmt_vpc_id != null ? 1 : 0
  route_table_id            = local.route_table_nat[0]
  destination_cidr_block    = var.mgmt_vpc_cidr != null ? var.mgmt_vpc_cidr : data.aws_vpc.mgmt_vpc[0].cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.mgmt_vpc_peering[0].id
}

resource "aws_route" "igwrt_mgmt_route" {
  count                     = local.mgmt_vpc_id != null ? 1 : 0
  route_table_id            = local.route_table_igw[0]
  destination_cidr_block    = var.mgmt_vpc_cidr != null ? var.mgmt_vpc_cidr : data.aws_vpc.mgmt_vpc[0].cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.mgmt_vpc_peering[0].id
}

resource "aws_route" "mgmt_route_back" {
  count                     = local.mgmt_vpc_id != null ? length(local.mgmt_vpc_rtables) : 0
  provider                  = aws.peer
  route_table_id            = element(local.mgmt_vpc_rtables, count.index)
  destination_cidr_block    = var.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.mgmt_vpc_peering[0].id
}

locals {
  kms_key_id_is_alias = var.kms_key_id != null && substr(var.kms_key_id == null ? "" : var.kms_key_id, 0, 12) != "arn:aws:kms:"
  kms_key_id          = ! local.kms_key_id_is_alias ? var.kms_key_id : data.aws_kms_key.enc_key[0].arn

  keypair = var.keypair != null ? var.keypair : aws_key_pair.kp[0].key_name
  vpc_id  = var.vpc_id != null ? var.vpc_id : aws_vpc.vpc[0].id

  core_root_disk_size          = var.core_root_disk_size != null ? var.core_root_disk_size : var.global_root_disk_size
  lb_root_disk_size            = var.lb_root_disk_size != null ? var.lb_root_disk_size : var.global_root_disk_size
  mongo_root_disk_size         = var.mongo_root_disk_size != null ? var.mongo_root_disk_size : var.global_root_disk_size
  kubemaster_root_disk_size    = var.kubemaster_root_disk_size != null ? var.kubemaster_root_disk_size : var.global_root_disk_size
  kubeworker_root_disk_size    = var.kubeworker_root_disk_size != null ? var.kubeworker_root_disk_size : var.global_root_disk_size
  agent_pub_root_disk_size     = var.agent_pub_root_disk_size != null ? var.agent_pub_root_disk_size : var.global_root_disk_size
  agent_private_root_disk_size = var.agent_private_root_disk_size != null ? var.agent_private_root_disk_size : var.global_root_disk_size

  core_root_disk_type          = var.core_root_disk_type != null ? var.core_root_disk_type : var.global_root_disk_type
  lb_root_disk_type            = var.lb_root_disk_type != null ? var.lb_root_disk_type : var.global_root_disk_type
  mongo_root_disk_type         = var.mongo_root_disk_type != null ? var.mongo_root_disk_type : var.global_root_disk_type
  kubemaster_root_disk_type    = var.kubemaster_root_disk_type != null ? var.kubemaster_root_disk_type : var.global_root_disk_type
  kubeworker_root_disk_type    = var.kubeworker_root_disk_type != null ? var.kubeworker_root_disk_type : var.global_root_disk_type
  agent_pub_root_disk_type     = var.agent_pub_root_disk_type != null ? var.agent_pub_root_disk_type : var.global_root_disk_type
  agent_private_root_disk_type = var.agent_private_root_disk_type != null ? var.agent_private_root_disk_type : var.global_root_disk_type

  ssh_ports = [
    {
      "proto"       = "tcp"
      "port"        = var.ssh_port
      "cidr_blocks" = var.ssh_allowed_cidr_blocks != null ? var.ssh_allowed_cidr_blocks : var.global_subnet_start_iprange
      "direction"   = "ingress"
    },
  ]

  az_names = var.az_names != null ? var.az_names : random_shuffle.az_names.result

  tmp_route_table_igw = var.vpc_id == null ? aws_vpc.vpc[0].default_route_table_id : var.route_table_igw
  tmp_route_table_nat = var.vpc_id == null ? aws_route_table.natgw_rtb[0].id : var.route_table_nat

  route_table_igw = local.tmp_route_table_igw != null ? [local.tmp_route_table_igw] : data.aws_route_table.igw.*.id
  route_table_nat = local.tmp_route_table_nat != null ? [local.tmp_route_table_nat] : data.aws_route_table.nat.*.id

  tmp_mgmt_vpc_id     = var.mgmt_vpc_id == "auto" ? data.aws_vpc.mgmt_vpc_auto[0].id : null
  mgmt_vpc_id         = local.tmp_mgmt_vpc_id != null ? local.tmp_mgmt_vpc_id : var.mgmt_vpc_id
  mgmt_vpc_region     = var.mgmt_vpc_region != null ? var.mgmt_vpc_region : data.aws_region.current.name
  mgmt_vpc_rtables    = length(var.mgmt_vpc_rtables) > 0 ? var.mgmt_vpc_rtables : data.aws_vpc.mgmt_vpc.*.main_route_table_id
  peer_in_same_region = local.mgmt_vpc_region == data.aws_region.current.name

  compute_subnet_start_iprange = var.compute_subnet_start_iprange != null ? var.compute_subnet_start_iprange : var.global_subnet_start_iprange
  data_subnet_start_iprange    = var.data_subnet_start_iprange != null ? var.data_subnet_start_iprange : var.global_subnet_start_iprange
  lb_subnet_start_iprange      = var.lb_subnet_start_iprange != null ? var.lb_subnet_start_iprange : var.global_subnet_start_iprange
  compute_subnet_newbits = abs(
    element(split("/", local.compute_subnet_start_iprange), 1) - var.subnet_length,
  )
  data_subnet_newbits = abs(
    element(split("/", local.data_subnet_start_iprange), 1) - var.subnet_length,
  )
  lb_subnet_newbits = abs(
    element(split("/", local.lb_subnet_start_iprange), 1) - var.subnet_length,
  )

  internal_communication_ports = var.internal_communication_ports != null ? var.internal_communication_ports : [
    {
      proto       = -1
      port        = 0
      cidr_blocks = var.global_subnet_start_iprange
      direction   = "ingress"
    },
    {
      proto       = -1
      port        = 0
      cidr_blocks = "0.0.0.0/0"
      direction   = "egress"
    },
  ]
  core_ports          = var.core_ports != null ? var.core_ports : local.internal_communication_ports
  mongo_ports         = var.mongo_ports != null ? var.mongo_ports : local.internal_communication_ports
  kubemaster_ports    = var.kubemaster_ports != null ? var.kubemaster_ports : local.internal_communication_ports
  kubeworker_ports    = var.kubeworker_ports != null ? var.kubeworker_ports : local.internal_communication_ports
  agent_pub_ports     = var.agent_pub_ports != null ? var.agent_pub_ports : local.internal_communication_ports
  agent_private_ports = var.agent_private_ports != null ? var.agent_private_ports : local.internal_communication_ports

  apply_roles            = var.datahub
  iam_kubeworker_profile = local.apply_roles ? var.iam_kubeworker_role != null ? var.iam_kubeworker_role : aws_iam_instance_profile.iam_kubeworker_profile[0].name : null
  iam_kubeworker_role    = local.apply_roles ? var.iam_kubeworker_role != null ? var.iam_kubeworker_role : aws_iam_role.iam_kubeworker_role[0].name : null

  listener_list = concat(var.cumulocity_elb_listeners,
    var.datahub ? var.datahub_elb_listeners : []
  )
}

