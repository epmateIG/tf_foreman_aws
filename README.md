# Terraform module creating a Cumulocity IoT environment (AWS)

## Goals

Create reproducible environment adhering to IaC approach using terraform.

Currently implemented:

  * Instance resource creation
  * EBS volume list
  * Storage formatting
  * ELB for load balancer nodes
  * Latest image autodiscovery
  * Workspaces support
  * Availability zones mapped to subnets

## Getting started

Steps:

  * Clone the c8y_envs directory
  * Copy the template (Empty-Template directory) to your new environment
  * Select or create an appropriate workspace: it will be used as prefix
  for the virtual machines created;
  * Specify the version of this module from Git;
  * Overwrite specific variables and provide values for ones that have
  no defaults;
  * Initialize your environment with `terraform init`;
  * Check the plan and/or apply it.

### Create TF env directory with workspace

Terraform always uses concept of modules and workspaces: default ones
are "root" and "default" respectively. Since we'll be using a module for
the provisioning, we need to separate it in it's own directory, so it's
called from the root module:

```
mkdir tf && cd tf
touch main.tf
terraform workspace new my-workspace ;# this command will also switch you
terraform workspace select my-workspace
terraform workspace list
```

Workspaces isolate state file - `terraform.tfstate`.
Workspace name will be used as a prefix for the environment both locally
and on AWS, so choose a suitable name for it.

Please do not use special and uppercase characters in your workspace name!

### Clone this repo or use it from git

Add this section (appropriately adjusted) to your main.tf created in the
previous section.
The syntax for usage with a versioned git repo is (replace the credentials
in url):

```
module "c8y_aws" {
  source                      = "git::ssh://git@bitbucket.org/m2m/tf_c8y_aws.git?ref=v3.2.0"
  global_subnet_start_iprange = "10.0.0.0/22"
  vpc_cidr                    = "10.0.0.0/16"
}
```

Example here provides two required variables, for the full list please refer
to `variables.tf`.

### Initialize the environment

If your environments (workspaces) share the backend, just init it:

```
terraform init
```

### Plan and apply

Check that everything is planned in a way you expected it, after you've
finished reviewing the changes apply it:

```
terraform apply
```

## Advanced usage

There are a lot of parameters in this module that depend on each other
and change the behaviour. If your use case is not described here, please
refer to `variables.tf` file for descriptions provided within.

### Public IPs

All loadbalancers will always have public IPs associated by design.

But there is a possibility to change that for all other node types, setting
`associate_public_ip` variable to `false`. In that case, the nodes without
the public IPs will need additional routing rules to access the Internet:

```
variable "route_table_nat" { type = string }
variable "route_table_igw" { type = string }
```

In case nat and igw routing tables are labeled with `tf_select_nat = true` and
`tf_select_igw = true` you can skip setting the variables.

### Encrypted root

Not provided by default in CentOS image, but you can use `ami` variable
to replace it with a custom image which provides the feature. In case
that image has a non-default "centos" user, replace `ami_default_user`
as well.

### Encrypted mongo volumes

Encrypted by default, toggle using `ebs_encrypt`.

### Root volume sizes

Can either be overwritten per node or provided globally for all nodes, accordingly:

`variable "global_root_disk_size" { type = number }` and `*_root_disk_size`.

### Changing IP addressing scheme

Refer to `*_startip`, `*_start_iprange` and `subnet_length` variables.

### Availability zones

Defaults to 3, discovered automatically. Controlled by `availability_zones`
variable.

Override is possible via `az_names` var of type list. By default, the zones
are randomized and filtered for availability starting from 2.1.1 release.

### Security groups

Refer to `*_ports` parameters and it's also possible to concat existing
with the `user_supplied_security_groups` var for easier modification.

## Development

### Testing

Run the tests with command:

```
go test -v -timeout=30m
```

Extending the tests: follow typical Go test structure (table-driven tests).
Add new test cases to the test case structure, they'll be executed in parallel.
