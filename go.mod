module bitbucket.org/m2m/tf_c8y_aws

go 1.13

require (
	github.com/gruntwork-io/terratest v0.26.0
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
