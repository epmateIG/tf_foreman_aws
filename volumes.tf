locals {
  core_vols_count         = length(var.core_aux_vols)
  mongo_vols_count        = length(var.mongo_aux_vols)
  kubemaster_vols_count   = length(var.kubemaster_aux_vols)
  kubeworker_vols_count   = length(var.kubeworker_aux_vols)
  loadbalancer_vols_count = length(var.loadbalancer_aux_vols)
}

resource "aws_ebs_volume" "core_aux_vol" {
  count             = var.core_nodes_count * local.core_vols_count
  availability_zone = element(local.az_names, floor(count.index / local.core_vols_count))
  size              = element(var.core_aux_vols, count.index)["size"]
  iops              = lookup(element(var.core_aux_vols, count.index), "iops", null)
  type              = element(var.core_aux_vols, count.index)["type"]
  encrypted         = element(var.core_aux_vols, count.index)["encrypt"]
  kms_key_id        = local.kms_key_id

  tags = merge({
    Name     = format("%s-core-vol-%03d", terraform.workspace, count.index + 1)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
    VolName  = element(var.core_aux_vols, count.index)["name"]
  }, var.user_supplied_tags)
}

resource "aws_volume_attachment" "core_ebs_attachment" {
  count       = var.core_nodes_count * local.core_vols_count
  device_name = element(var.core_aux_vols, count.index)["device"]
  volume_id   = element(aws_ebs_volume.core_aux_vol.*.id, count.index)
  instance_id = element(aws_instance.core.*.id, floor(count.index / local.core_vols_count))
}


resource "aws_ebs_volume" "mongo_aux_vol" {
  count             = var.mongo_nodes_count * local.mongo_vols_count
  availability_zone = element(local.az_names, floor(count.index / local.mongo_vols_count))
  size              = element(var.mongo_aux_vols, count.index)["size"]
  iops              = lookup(element(var.mongo_aux_vols, count.index), "iops", null)
  type              = element(var.mongo_aux_vols, count.index)["type"]
  encrypted         = element(var.mongo_aux_vols, count.index)["encrypt"]
  kms_key_id        = local.kms_key_id

  tags = merge({
    Name     = format("%s-mongo-vol-%03d", terraform.workspace, count.index + 1)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
    VolName  = element(var.mongo_aux_vols, count.index)["name"]
  }, var.user_supplied_tags)
}

resource "aws_volume_attachment" "mongo_ebs_attachment" {
  count       = var.mongo_nodes_count * local.mongo_vols_count
  device_name = element(var.mongo_aux_vols, count.index)["device"]
  volume_id   = element(aws_ebs_volume.mongo_aux_vol.*.id, count.index)
  instance_id = element(aws_instance.mongo.*.id, floor(count.index / local.mongo_vols_count))
}

resource "aws_ebs_volume" "kubemaster_aux_vol" {
  count             = var.kubemaster_nodes_count * local.kubemaster_vols_count
  availability_zone = element(local.az_names, floor(count.index / local.kubemaster_vols_count))
  size              = element(var.kubemaster_aux_vols, count.index)["size"]
  iops              = lookup(element(var.kubemaster_aux_vols, count.index), "iops", null)
  type              = element(var.kubemaster_aux_vols, count.index)["type"]
  encrypted         = element(var.kubemaster_aux_vols, count.index)["encrypt"]
  kms_key_id        = local.kms_key_id

  tags = merge({
    Name     = format("%s-kubemaster-vol-%03d", terraform.workspace, count.index + 1)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
    VolName  = element(var.kubemaster_aux_vols, count.index)["name"]
  }, var.user_supplied_tags)
}

resource "aws_volume_attachment" "kubemaster_ebs_attachment" {
  count       = var.kubemaster_nodes_count * local.kubemaster_vols_count
  device_name = element(var.kubemaster_aux_vols, count.index)["device"]
  volume_id   = element(aws_ebs_volume.kubemaster_aux_vol.*.id, count.index)
  instance_id = element(aws_instance.kubemaster.*.id, floor(count.index / local.kubemaster_vols_count))
}

resource "aws_ebs_volume" "kubeworker_aux_vol" {
  count             = var.kubeworker_nodes_count * local.kubeworker_vols_count
  availability_zone = element(local.az_names, floor(count.index / local.kubeworker_vols_count))
  size              = element(var.kubeworker_aux_vols, count.index)["size"]
  iops              = lookup(element(var.kubeworker_aux_vols, count.index), "iops", null)
  type              = element(var.kubeworker_aux_vols, count.index)["type"]
  encrypted         = element(var.kubeworker_aux_vols, count.index)["encrypt"]
  kms_key_id        = local.kms_key_id

  tags = merge({
    Name     = format("%s-kubeworker-vol-%03d", terraform.workspace, count.index + 1)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
    VolName  = element(var.kubeworker_aux_vols, count.index)["name"]
  }, var.user_supplied_tags)
}

resource "aws_volume_attachment" "kubeworker_ebs_attachment" {
  count       = var.kubeworker_nodes_count * local.kubeworker_vols_count
  device_name = element(var.kubeworker_aux_vols, count.index)["device"]
  volume_id   = element(aws_ebs_volume.kubeworker_aux_vol.*.id, count.index)
  instance_id = element(aws_instance.kubeworker.*.id, floor(count.index / local.kubeworker_vols_count))
}

resource "aws_ebs_volume" "loadbalancer_aux_vol" {
  count             = var.loadbalancer_nodes_count * local.loadbalancer_vols_count
  availability_zone = element(local.az_names, floor(count.index / local.loadbalancer_vols_count))
  size              = element(var.loadbalancer_aux_vols, count.index)["size"]
  iops              = lookup(element(var.loadbalancer_aux_vols, count.index), "iops", null)
  type              = element(var.loadbalancer_aux_vols, count.index)["type"]
  encrypted         = element(var.loadbalancer_aux_vols, count.index)["encrypt"]
  kms_key_id        = local.kms_key_id

  tags = merge({
    Name     = format("%s-loadbalancer-vol-%03d", terraform.workspace, count.index + 1)
    Customer = var.customer_name != null ? var.customer_name : terraform.workspace
    VolName  = element(var.loadbalancer_aux_vols, count.index)["name"]
  }, var.user_supplied_tags)
}

resource "aws_volume_attachment" "loadbalancer_ebs_attachment" {
  count       = var.loadbalancer_nodes_count * local.loadbalancer_vols_count
  device_name = element(var.loadbalancer_aux_vols, count.index)["device"]
  volume_id   = element(aws_ebs_volume.loadbalancer_aux_vol.*.id, count.index)
  instance_id = element(aws_instance.loadbalancer.*.id, floor(count.index / local.loadbalancer_vols_count))
}
