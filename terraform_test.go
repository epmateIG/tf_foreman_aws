package main

import (
	"github.com/gruntwork-io/terratest/modules/terraform"
	"os"
	"path/filepath"
	"testing"
)

func linkAll(patterns []string, dst string) error {
	var files []string
	for _, pattern := range patterns {
		fs, err := filepath.Glob(pattern)
		if err != nil {
			return err
		}
		files = append(files, fs...)
	}

	for _, f := range files {
		if err := os.Link(f, filepath.Join(dst, f)); err != nil {
			return err
		}
	}

	return nil
}

func TestC8YAWS(t *testing.T) {
	t.Parallel()
	tcs := []struct {
		name      string
		workspace string
		tfvars    []string
		env       map[string]string
		outputs   []string
		want      func(string) bool
	}{
		{
			name:      "General defaults",
			workspace: "general",
			tfvars:    []string{"../testdata/general.tfvars"},
			env: map[string]string{
				"AWS_DEFAULT_REGION": "eu-north-1",
			},
			outputs: []string{
				"core_ids",
				"loadbalancer_ids",
				"mongo_ids",
				"kubemaster_ids",
				"kubeworker_ids",
				"agent_pub_ids",
				"agent_private_ids",
			},
			want: func(o string) bool {
				return true
			},
		},
		{
			name:      "Custom subnet length test",
			workspace: "subnets",
			tfvars:    []string{"../testdata/general.tfvars", "../testdata/custom_subnet.tfvars"},
			env: map[string]string{
				"AWS_DEFAULT_REGION": "us-east-2",
			},
			outputs: []string{},
			want: func(o string) bool {
				return true
			},
		},
		{
			name:      "User-supplied tags test",
			workspace: "customtags",
			tfvars:    []string{"../testdata/general.tfvars", "../testdata/custom_tags.tfvars"},
			env: map[string]string{
				"AWS_DEFAULT_REGION": "eu-west-3",
			},
			outputs: []string{},
			want: func(o string) bool {
				return true
			},
		},
	}

	for _, tc := range tcs {
		// copy the value for parallel execution
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			tfdir := tc.workspace
			if err := os.MkdirAll(tfdir, 0755); err != nil {
				t.Errorf("Error creating directory: %v\n", err)
				return
			}
			if err := linkAll([]string{"*.tf", "*.tmpl"}, tfdir); err != nil {
				t.Errorf("Error linking the files: %v\n", err)
				return
			}
			defer os.RemoveAll(tfdir)
			tfOpts := &terraform.Options{
				TerraformDir: tfdir,
				VarFiles:     tc.tfvars,
				EnvVars:      tc.env,
			}
			terraform.WorkspaceSelectOrNew(t, tfOpts, tc.workspace)
			defer terraform.Destroy(t, tfOpts)

			terraform.InitAndApply(t, tfOpts)

			for _, output := range tc.outputs {
				val := terraform.Output(t, tfOpts, output)
				t.Logf("Got output %s value: %v\n", output, val)
				if len(val) == 0 {
					t.Errorf("Apply failed to create: %s\n", output)
				}
			}
		})
	}
}
