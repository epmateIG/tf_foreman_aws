output "vpc_id" {
  value = local.vpc_id
}

output "kp_private_key" {
  value = tls_private_key.pk.*.private_key_pem
}

output "core_public_addresses" {
  value = aws_instance.core.*.public_ip
}

output "core_private_addresses" {
  value = aws_instance.core.*.private_ip
}

output "mongo_public_addresses" {
  value = aws_instance.mongo.*.public_ip
}

output "mongo_private_addresses" {
  value = aws_instance.mongo.*.private_ip
}

output "loadbalancer_public_addresses" {
  value = aws_instance.loadbalancer.*.public_ip
}

output "loadbalancer_private_addresses" {
  value = aws_instance.loadbalancer.*.private_ip
}

output "kubemaster_public_addresses" {
  value = aws_instance.kubemaster.*.public_ip
}

output "kubemaster_private_addresses" {
  value = aws_instance.kubemaster.*.private_ip
}

output "kubeworker_public_addresses" {
  value = aws_instance.kubeworker.*.public_ip
}

output "kubeworker_private_addresses" {
  value = aws_instance.kubeworker.*.private_ip
}

output "agent_pub_public_addresses" {
  value = aws_instance.agent_pub.*.public_ip
}

output "agent_pub_private_addresses" {
  value = aws_instance.agent_pub.*.private_ip
}

output "agent_private_private_addresses" {
  value = aws_instance.agent_private.*.private_ip
}

output "core_ids" {
  value = aws_instance.core.*.id
}

output "loadbalancer_ids" {
  value = aws_instance.loadbalancer.*.id
}

output "mongo_ids" {
  value = aws_instance.mongo.*.id
}

output "kubemaster_ids" {
  value = aws_instance.kubemaster.*.id
}

output "kubeworker_ids" {
  value = aws_instance.kubeworker.*.id
}

output "agent_pub_ids" {
  value = aws_instance.agent_pub.*.id
}

output "agent_private_ids" {
  value = aws_instance.agent_private.*.id
}

output "ami_default_user" {
  value = var.ami_default_user
}

output "data_subnets" {
  value = aws_subnet.data.*.id
}

output "data_cidr_blocks" {
  value = aws_subnet.data.*.cidr_block
}

output "compute_subnets" {
  value = aws_subnet.compute.*.id
}

output "compute_cidr_blocks" {
  value = aws_subnet.compute.*.cidr_block
}

output "lb_subnets" {
  value = aws_subnet.lb.*.id
}

output "lb_cidr_blocks" {
  value = aws_subnet.lb.*.cidr_block
}

output "mongo_names" {
  value = aws_instance.mongo.*.tags.Name
}

output "core_names" {
  value = aws_instance.core.*.tags.Name
}

output "kubemaster_names" {
  value = aws_instance.kubemaster.*.tags.Name
}

output "kubeworker_names" {
  value = aws_instance.kubeworker.*.tags.Name
}

output "loadbalancer_names" {
  value = aws_instance.loadbalancer.*.tags.Name
}

output "agent_pub_names" {
  value = aws_instance.agent_pub.*.tags.Name
}

output "agent_private_names" {
  value = aws_instance.agent_private.*.tags.Name
}

output "iam_kubeworker_role" {
  value = local.iam_kubeworker_role
}

